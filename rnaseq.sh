#!/bin/bash
. ./pipeline_functions.sh

##################################################
### Software requirements 1
### For the software: FastQC, TrimGalore!, CutAdapt, STAR
### Provide the paths to the softare (i.e. the directory where the executable is located)
### (It is not necessary if it's already installed and added to your PATH)
##################################################

path_fastqc='/path/to/FastQC/'
path_trimgalore='/path/to/TrimGalore/'
path_cutadapt='/path/to/CutAdapt/'
path_cufflinks='/path/to/Cufflinks'
path_STAR='/path/to/STAR/'

##################################################
### Software requirements 2
### For the software: PICARD and GATK
### Provide the full paths to the jar
##################################################

software_picard='/path/to/picard.jar'
software_gatk='/path/to/GenomeAnalysisTK.jar'

# The links to download the software above
#https://www.bioinformatics.babraham.ac.uk/projects/download.html#fastqc
#https://www.bioinformatics.babraham.ac.uk/projects/download.html#trim_galore
#http://cutadapt.readthedocs.io/en/stable/guide.html
#http://cole-trapnell-lab.github.io/cufflinks/install/
#https://github.com/alexdobin/STAR/releases
#https://broadinstitute.github.io/picard/
#https://software.broadinstitute.org/gatk/download/archive

##################################################
### Provide the paths to the reference genome, the gtf (Gene transfer format) file
### and the vcf (variant) file for the GATK pipeline
### For gtf file: If you are working with human and is using the hg19 reference genome, we recommend you the use the hg19-M.gtf hosted on our Github, that contains chrM as well
### For vcf file: Visit the following GATK website to find out which vcf file should you use 
### https://gatkforums.broadinstitute.org/gatk/discussion/1247/what-should-i-use-as-known-variants-sites-for-running-tool-x 
##################################################

ref_genome="/path/to/genome.fasta"
gtf_file="/path/to/gtffile.gtf"

knownSitesRealigner="/path/to/realigner.vcf"
knownSitesRecal="/path/to/recalibration.vcf"
knownSitesCaller="/path/to/mutation_calling.vcf"

##################################################
### Details of you samples
##################################################

# Provide the path to each replicate of your NORMAL/CONTROL sample in a list (just the directory, excluding the fastq file)
sample_normal=("/path/to/normal_replicate_1" "/path/to/normal_replicate_2" "/path/to/normal_replicate_3")

# Provide the path to each replicate of your DISEASE/CONDITION sample in a list (just the directory, excluding the fastq file)
sample_disease=("/path/to/disease_replicate_1" "/path/to/disease_replicate_2" "/path/to/disease_replicate_3")

# Is it a single- or paired-end read?
# 1 for single
# 2 for paired
sample_read=1

##################################################
### The directory to put the intermediate and final output of the script
##################################################
dir_output="/path/to/output/directory"

##################################################
### The functions from pipeline_function.sh are called
### Comment out the functions you don't need
### For the detail of each function, please check pipeline_function.sh
### It's NECESSARY to run AddPath, FileCheck and CreateOutputDir EVERYTIME for the script to proceed smoothly
##################################################

# This adds the paths to the software (as filled above) to the current session so the script could run with the softwares
AddPath

# This checks if all the necessary target files are provided for the script
FileCheck

# This creates output directories for the intermediate and final output of the script
CreateOutputDir

# This checks the quality of your fastq files and gives a report
QualityControl

# This performs mapping using STAR aligner
Mapping

# This performs differential expression with cuffdiff
DiffExp

# This performs the GATK best practice pipeline to call mutations, by comparing the normal and diseased sample
Mutation

