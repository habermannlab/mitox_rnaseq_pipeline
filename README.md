# Expression and Mutation Analysis Pipeline for RNA-seq Data used in the mitoXplorer project

The shell scripts rnaseq.sh and pipeline_functions.sh perform both expression and mutation analysis on RNA-seq data of paired samples (with or without replicates). 

The output of this scripts, which include both expression and mutation files are in a format that is ready to be fed to MitoXplorer. 

The usage of the script, software requirement and the required input are described below. 


## Usage of the scripts

The scripts is well-documented and users are recommended to go through the script and make sure they know what they are doing.
Users have to download both rnaseq.sh and pipeline_functions.sh and place them under the same directory

rnaseq.sh is the main script where users could provide the path to the software and input files.
It calls pipeline_functions.sh, which contains functions for rnaseq.sh to run. They are:

* **AddPath**: This adds the paths to the software (as filled above) to the current session so the script could run with the softwares

* **FileCheck**: This checks if all the necessary target files are provided for the script

* **CreateOutputDir**: This creates output directories for the intermediate and final output of the script

* **QualityControl**: This checks the quality of your fastq files and gives a report

* **Mapping**: This performs mapping using STAR aligner

* **DiffExp**: This performs differential expression with Cuffdiff

* **Mutation**: This runs the GATK best practice pipeline to call mutations, and compares between the normal and diseased sample to give unique mutations

A complete RNA-seq analysis will run through all the above functions.
To start the analysis, simply fill in all the information within rnaseq.sh
At the terminal, change your current directory to the one where the two scripts are located:

    cd /path/to/directory/of/rnaseq.sh+pipeline_functions.sh

and run:

    . rnaseq.sh


## Software requirements

The following softwares are required to run the scripts:

* **FastQC** [(Download)](https://www.bioinformatics.babraham.ac.uk/projects/download.html#fastqc)

* **TrimGalore!** [(Download)](https://www.bioinformatics.babraham.ac.uk/projects/download.html#trim_galore)

* **CutAdapt** [(Download)](http://cutadapt.readthedocs.io/en/stable/guide.html)

* **Cufflinks** [(Download)](http://cole-trapnell-lab.github.io/cufflinks/install/)

* **STAR aligner** [(Download)](https://github.com/alexdobin/STAR/releases)

* **Picard** [(Download)](https://broadinstitute.github.io/picard/)

* **GATK tool** [(Download)](https://software.broadinstitute.org/gatk/download/archive)

For GATK tool, the script currently uses GATK 3.8.1. It will be updated to GATK 4 soon as this version becomes stable with documentation provided

At rnaseq.sh, Users have to provide the path to the above software (for Picard and GATK, the full path of the jar file):

    path_fastqc='/path/to/fastqc'
    ...
(If the softwares have been installed globally and added to the Path, then this is not necessary)

    software_picard='/path/to/picard.jar'
    ...

(The instructions on how to fill the above is documented at rnaseq.sh also)

## Required Input

### Samples

Since the script is running differential expression analysis, both NORMAL and DISEASED samples are needed for comparison
Normal refers to normal or control samples. Disease referes to diseased samples or samples with conditions.

At rnaseq.sh, Users have to provide the path where each replicate of the samples are located (excluding the fastq file in the path).
The number of replicates is not limited.

At rnaseq.sh:

    sample_normal=("/path/to/normal_replicate_1" "/path/to/normal_replicate_2" "/path/to/normal_replicate_3")

    sample_disease=("/path/to/disease_replicate_1" "/path/to/disease_replicate_2" "/path/to/disease_replicate_3")

Users also have to indicate whether the sample is single- or paired-end (1 for single, 2 for paired):

    sample_read=1

An output directory, that stores all the intermediate and final out also has to be provided:

    dir_output="/path/to/output/directory"


### Other files

As alignment will be performed, a reference genome and gtf file have to be provided

At rnaseq.sh:

    ref_genome="/path/to/genome.fasta"
    gtf_file="/path/to/gtffile.gtf"

As part of the GATK best practice pipeline, variant files (.vcf) also has to be provided. To find out which vcf file you should use for Realignment, Recalibration and Mutation Calling, visit this link (which provides links to download the vcf files as well):
[https://gatkforums.broadinstitute.org/gatk/discussion/1247/what-should-i-use-as-known-variants-sites-for-running-tool-x](https://gatkforums.broadinstitute.org/gatk/discussion/1247/what-should-i-use-as-known-variants-sites-for-running-tool-x)

At rnaseq.sh:

    knownSitesRealigner="/path/to/realigner.vcf"
    knownSitesRecal="/path/to/recalibration.vcf"
    knownSitesCaller="/path/to/mutation_calling.vcf"

(The instructions on how to fill the above is documented at rnaseq.sh also)
